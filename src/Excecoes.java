import java.io.IOException;
import java.util.Scanner;

public class Excecoes {
	public static void main(String[] args) {
		Arquivo arquivo1 = new Arquivo();
		
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Digite o nome do arquivo:");
		String nomeDoArquivo = leitor.nextLine();
		
		try {
			arquivo1.imprimeArquivo(nomeDoArquivo);
		} catch (IOException e) {
			System.out.println("O arquivo " + nomeDoArquivo + " não pode ser aberto.");
			//e.printStackTrace();
		}
		
	}
}
