import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Arquivo {
	private FileInputStream arquivo = null;
	
	public void imprimeArquivo(String nomeDoArquivo) throws IOException, FileNotFoundException {
		arquivo = new FileInputStream(nomeDoArquivo);
		
		int c;
		while((c = arquivo.read()) != -1) {
			System.out.print((char) c);
		}
		
		arquivo.close();
	}
}
